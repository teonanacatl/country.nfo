import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CountryModel } from '../models/country.model';
import { RegionModel } from '../models/region.model';

@Injectable({
    providedIn: 'root',
})
export class AppApiService {
    private _rootUrl: string = 'https://restcountries.eu/rest/v2';

    constructor(private _httpClient: HttpClient) {}

    getRegionsList(): Observable<RegionModel[]> {
        return this._httpClient.get('/assets/data/regions.json') as Observable<
            RegionModel[]
        >;
    }

    getCountriesList(regionCode: string): Observable<CountryModel[]> {
        return this._httpClient.get(
            `${this._rootUrl}/region/${regionCode}`
        ) as Observable<CountryModel[]>;
    }

    getCountryItem(id: string): Observable<CountryModel[]> {
        return this._httpClient.get(
            `${this._rootUrl}/alpha?codes=${id}`
        ) as Observable<CountryModel[]>;
    }
}
