import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppRegionsComponent } from './app-regions.component';

describe('AppRegionsComponent', () => {
  let component: AppRegionsComponent;
  let fixture: ComponentFixture<AppRegionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppRegionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppRegionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
