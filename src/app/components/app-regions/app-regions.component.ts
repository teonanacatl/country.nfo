import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { RegionModel } from 'src/app/models/region.model';
import { SubscriptionMixin } from 'src/app/modules/shared/mixins/subscription.mixin';
import { AppApiService } from 'src/app/services/app-api.service';

@Component({
    selector: 'app-regions',
    templateUrl: './app-regions.component.html',
    styleUrls: ['./app-regions.component.scss'],
})
export class AppRegionsComponent extends SubscriptionMixin implements OnInit {
    regionCode: string | undefined;
    regions: RegionModel[] | undefined;

    @Output()
    clickEventEmmiter: EventEmitter<string> = new EventEmitter<string>();

    constructor(private _appApiService: AppApiService) {
        super();
    }

    ngOnInit(): void {
        this._fetch();
    }

    onClick(regionCode: string): void {
        this.clickEventEmmiter.emit(regionCode);
        this.regionCode = regionCode;
    }

    private _fetch(): void {
        this._appApiService
            .getRegionsList()
            .pipe(takeUntil(this.finish$))
            .subscribe((response: RegionModel[]): void => {
                this.regions = response;
            });
    }
}
