import { Component, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { CountryModel } from 'src/app/models/country.model';

@Component({
    selector: 'app-main',
    templateUrl: './app-main.component.html',
    styleUrls: ['./app-main.component.scss'],
})
export class AppMainComponent implements OnInit {
    form: FormGroup | undefined;

    constructor() {}

    ngOnInit(): void {
        this._initForm();
        this._setForm();
    }

    onRegionClick(regionCode: string, stepper: MatStepper): void {
        this.regionAbstractControl?.setValue(regionCode);
        stepper.next();
    }

    onCountryClick(country: CountryModel, stepper: MatStepper): void {
        this.countryAbstractControl?.setValue(country);
        stepper.next();
    }

    private _initForm(): void {
        this.form = new FormGroup({
            region: new FormGroup({
                field: new FormControl(),
            }),
            country: new FormGroup({
                field: new FormControl(),
            }),
        });
    }

    private _setForm(): void {
        this.countryAbstractControl?.setValidators([Validators.required]);
        this.regionAbstractControl?.setValidators([Validators.required]);
    }

    get countryAbstractControl(): AbstractControl | null | undefined {
        return this.form?.get('country.field');
    }

    get countryFormGroup(): FormGroup {
        return this.form?.get('country') as FormGroup;
    }

    get regionAbstractControl(): AbstractControl | null | undefined {
        return this.form?.get('region.field');
    }

    get regionFormGroup(): FormGroup {
        return this.form?.get('region') as FormGroup;
    }
}
