import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output,
    SimpleChanges,
} from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { CountryModel } from 'src/app/models/country.model';
import { SubscriptionMixin } from 'src/app/modules/shared/mixins/subscription.mixin';
import { AppApiService } from 'src/app/services/app-api.service';

@Component({
    selector: 'app-countries',
    templateUrl: './app-countries.component.html',
    styleUrls: ['./app-countries.component.scss'],
})
export class AppCountriesComponent
    extends SubscriptionMixin
    implements OnChanges
{
    @Output()
    clickEventEmmiter: EventEmitter<CountryModel> = new EventEmitter<CountryModel>();

    country: CountryModel | undefined;

    regions: CountryModel[] | undefined;
    countries: CountryModel[] | undefined;
    flags: CountryModel[] | undefined;

    @Input()
    regionCode: string | undefined;

    threeRows = 100 / 3 + '%';

    constructor(private _appApiService: AppApiService) {
        super();
    }

    ngOnChanges(simpleChanges: SimpleChanges): void {
        if (
            simpleChanges.regionCode.currentValue !==
            simpleChanges.regionCode.previousValue
        ) {
            this._fetch();
        }
    }

    onClick(country: CountryModel): void {
        this.clickEventEmmiter.emit(country);
        this.country = country;
    }

    private _fetch(): void {
        this._appApiService
            .getCountriesList(this.regionCode as string)
            .pipe(takeUntil(this.finish$))
            .subscribe((response: CountryModel[]): void => {
                this.countries = response;
            });
    }
}
