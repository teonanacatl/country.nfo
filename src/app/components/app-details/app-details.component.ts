import { Component, Input } from '@angular/core';
import { CountryModel } from 'src/app/models/country.model';

@Component({
    selector: 'app-details',
    templateUrl: './app-details.component.html',
    styleUrls: ['./app-details.component.scss'],
})
export class AppDetailsComponent {
    @Input()
    country: CountryModel | undefined;

    constructor() {}
}
