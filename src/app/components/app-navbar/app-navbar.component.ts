import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppAboutComponent } from '../app-about/app-about.component';

@Component({
    selector: 'app-navbar',
    templateUrl: './app-navbar.component.html',
    styleUrls: ['./app-navbar.component.scss'],
})
export class AppNavbarComponent {
    constructor(private _matDialog: MatDialog) {}

    openAboutMeDialog(): void {
        const matDialogRef: MatDialogRef<AppAboutComponent> =
            this._matDialog.open(AppAboutComponent);
    }
}
