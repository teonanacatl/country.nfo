import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppNavbarComponent } from './app-navbar.component';

describe('AppNavbarComponent', () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [AppNavbarComponent],
        }).compileComponents();
    });

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppNavbarComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });
});
