export interface RegionModel {
    name: string;
    code: string;
}
