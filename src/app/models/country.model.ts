import { CurrencyModel } from './currency.model';

export interface CountryModel {
    name: string;
    capital: string;
    population: number;
    flag: string;
    subregion: string;
    region: string;
    nativeName: string;
    currencies: CurrencyModel[];
}
