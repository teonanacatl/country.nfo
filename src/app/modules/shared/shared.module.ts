import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VendorsModule } from './modules/vendors/vendors.module';

@NgModule({
    declarations: [],
    imports: [CommonModule, VendorsModule, RouterModule, ReactiveFormsModule],
    exports: [VendorsModule, RouterModule, ReactiveFormsModule],
})
export class SharedModule {}
