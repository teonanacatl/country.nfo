import { Directive as Mixin, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Mixin()
export class SubscriptionMixin implements OnDestroy {
    finish$: Subject<void> = new Subject<void>();

    ngOnDestroy(): void {
        this.finish$.next();
    }
}
