import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppAboutComponent } from './components/app-about/app-about.component';
import { AppCountriesComponent } from './components/app-countries/app-countries.component';
import { AppDetailsComponent } from './components/app-details/app-details.component';
import { AppMainComponent } from './components/app-main/app-main.component';
import { AppNavbarComponent } from './components/app-navbar/app-navbar.component';
import { AppRegionsComponent } from './components/app-regions/app-regions.component';
import { SharedModule } from './modules/shared/shared.module';

@NgModule({
    declarations: [
        AppMainComponent,
        AppNavbarComponent,
        AppAboutComponent,
        AppRegionsComponent,
        AppCountriesComponent,
        AppDetailsComponent,
    ],
    imports: [BrowserModule, AppRoutingModule, SharedModule, HttpClientModule],
    providers: [],
    bootstrap: [AppMainComponent],
})
export class AppModule {}
